<?php defined('ABSPATH') || exit;

get_header();
?>

<main class="c_main">
  <div class="l_container l_container--narrow">
    <section class="c_section c_section--fullheight">
      <h1>Error 404</h1>
      <p>Sorry, we couldn't find what you're looking for. Please try our <a href="<?= site_url(); ?>" style="color:#8560cf; font-weight:700">homepage</a>.</p>
    </section>
  </div>
</div>

<?php get_footer(); ?>
