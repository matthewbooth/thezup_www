<?php defined('ABSPATH') || exit;

$other_posts = new WP_Query(array(
	'posts_per_page' => 2,
	'post_type' => 'post',
	'post__not_in' => [$post->ID]
));

get_header();
the_post();
?>

<main class="c_main">
	<section class="c_section c_section--white">
  	<div class="l_container">

			<?php the_content(); ?>

			<?php if (get_field('bullet_points')): foreach (get_field('bullet_points') as $key => $value): ?>

				<div class="c_callout">
					<h3 class="c_callout__title"><span class="c_callout__number"><?= str_pad(++$key, 3, "0", STR_PAD_LEFT); ?></span> <?php echo $value['heading'] ?></h3>
					<p><?php echo $value['body_copy'] ?></p>
				</div>

			<?php endforeach; endif; ?>

		</div>
	</section>

	<?php if ($other_posts->have_posts()): ?>

		<section class="c_section c_section--alt">
			<div class="l_container">
				<h2 style="text-align:center">Further Reading.</h2>
				<div class="l_related">

					<?php
					while ($other_posts->have_posts()) {
						$other_posts->the_post();
						get_template_part("template-parts/post/loop");
					}
					wp_reset_query();
					?>

				</div>
			</div>
		</section>

	<?php endif; ?>

	<?php get_template_part("template-parts/cta"); ?>

</main>

<?php
get_template_part("template-parts/footer");
get_footer();
?>
