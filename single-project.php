<?php defined('ABSPATH') || exit;

$other_projects = new WP_Query(array(
	'posts_per_page' => 2,
	'post_type' => 'project',
	'post__not_in' => [$post->ID]
));

get_header();
the_post();
?>

<main class="c_main">
	<section class="c_section c_section--white">
		<div class="l_container">

			<?php the_content(); ?>

			<?php if (get_field('images')): ?>

				<div class="l_project-gallery">

					<?php
					foreach (get_field('images') as $image) {
						printf('<div class="l_project-gallery__item" data-aos="fade-up">%s</div>', wp_get_attachment_image($image['ID'], 'medium_large', false, array('class' => 'lazyload')));
					}
					?>

				</div>

			<?php endif; ?>

		</div>
	</section>

	<?php if ($other_projects->have_posts()): ?>

		<section class="c_section c_section--alt">
			<div class="l_container l_container--wide">
				<h2 style="text-align:center">Other Projects.</h2>
				<div class="l_related">

					<?php
					while ($other_projects->have_posts()) {
						$other_projects->the_post();
						get_template_part("template-parts/project/loop");
					}
					wp_reset_query();
					?>

				</div>
			</div>
		</section>

	<?php endif; ?>

	<?php get_template_part("template-parts/cta"); ?>

</main>

<?php
get_template_part("template-parts/footer");
get_footer();
?>
