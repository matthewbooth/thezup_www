<?php defined('ABSPATH') || exit; ?>

<nav class="c_primary-navigation">
  <ul class="c_primary-navigation__list">
    <li class="c_primary-navigation__item c_primary-navigation__item--desktop"><a class="c_primary-navigation__link" href="<?= get_post_type_archive_link('post'); ?>">Blog</a></li>
    <li class="c_primary-navigation__item c_primary-navigation__item--desktop"><a class="c_primary-navigation__link" href="<?= get_post_type_archive_link('project'); ?>">Projects</a></li>
    <li class="c_primary-navigation__item c_primary-navigation__item--desktop"><a class="c_primary-navigation__link c_primary-navigation__link--highlight" href="#cta">Contact</a></li>
    <li class="c_primary-navigation__item c_primary-navigation__item--mobile">
      <a href="#" class="c_primary-navigation__link js_gutter-toggle" data-target="gutter-menu" title="Open Mobile Navigation">
        <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
          <line x1="3" y1="12" x2="21" y2="12"></line>
          <line x1="3" y1="6" x2="21" y2="6"></line>
          <line x1="3" y1="18" x2="21" y2="18"></line>
        </svg>
      </a>
    </li>
  </ul>
</nav>
