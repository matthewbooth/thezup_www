<div class="c_gutter-navigation">
  <a href="#" class="c_gutter-navigation__close js_gutter-toggle" data-target="gutter-menu" title="Close Mobile Navigation">&times;</a>
  <nav>
    <ul class="c_gutter-navigation__list">
      <li class="c_gutter-navigation__item"><a class="c_gutter-navigation__link" href="<?= site_url(); ?>" title="Home">Home</a></li>
      <li class="c_gutter-navigation__item"><a class="c_gutter-navigation__link" href="<?= get_post_type_archive_link('post'); ?>" title="Blog">Blog</a></li>
      <li class="c_gutter-navigation__item"><a class="c_gutter-navigation__link" href="<?= get_post_type_archive_link('project'); ?>" title="Projects">Projects</a></li>
      <li class="c_gutter-navigation__item"><a class="c_gutter-navigation__link c_gutter-navigation__link--highlight js_gutter-toggle" data-target="gutter-menu" href="#cta" title="Contact">Contact</a></li>
    </ul>
  </nav>
</div>
