<?php defined('ABSPATH') || exit; ?>

<?php // the_field('post_footer', 'options'); ?>
<div class="c_cta" id="cta">
  <div class="l_container">
    <p class="c_cta__message" data-aos="fade" data-aos-duration="900" data-aos-delay="300">We help our clients define the future they want and then we ensure they achieve it. If you would like to know more about how we could help through doing this for you, then please <a class="c_cta__link" href="mailto:now@thezup.com">get in touch</a>.</p>
  </div>
</div>
