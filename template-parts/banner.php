<?php defined('ABSPATH') || exit; ?>

<header class="c_banner">
  <div class="c_banner__layout l_container">
    <div class="c_banner__branding">

      <?php if (!is_front_page()) get_template_part("template-parts/branding"); ?>

    </div>
    <div class="c_banner__navigation">

      <?php get_template_part("template-parts/navigation", "primary"); ?>

    </div>
  </div>
</header>
