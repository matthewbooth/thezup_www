<?php defined('ABSPATH') || exit; ?>

<a class="c_project" data-aos="fade-up" href="<?php the_permalink(); ?>">

  <?php the_post_thumbnail('post_preview', array('class' => 'c_project__image')); ?>

  <div class="c_project__summary">
    <h3 class="c_project__headline"><?php the_title(); ?></h3>
  </div>
</a>
