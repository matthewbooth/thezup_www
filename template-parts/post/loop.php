<?php defined('ABSPATH') || exit; ?>

<a class="c_post" data-aos="fade-up" href="<?php the_permalink(); ?>">

  <?php the_post_thumbnail('post_preview', array('class' => 'c_post__image')); ?>

  <div class="c_post__summary">
    <h3 class="c_post__headline"><?php the_title(); ?></h3>
    <p class="c_post__date"><?php the_date(); ?></p>
    <div class="c_post__excerpt">

      <?php the_excerpt(); ?>

    </div>
  </div>
</a>
