<?php defined('ABSPATH') || exit; ?>

<div class="c_hero">
  <div class="l_container" data-aos="fade" data-aos-duration="500" data-aos-delay="500">

    <?php if (is_front_page()): ?>

      <?php get_template_part("template-parts/branding"); ?>

    <?php else: ?>

      <h1 class="c_hero__title"><?php the_title(); ?></h1>

    <?php endif; ?>

  </div>
</div>
