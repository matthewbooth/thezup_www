<?php defined('ABSPATH') || exit; ?>

<footer class="c_footer">
  <div class="l_container">
    <h2 class="c_footer__title"><?php the_field('cta_headline', 'options') ?></h2>
    <nav class="c_footer-navigation">
      <ul class="c_footer-navigation__list">

        <?php
        ZUP::optional_field('<li class="c_footer-navigation__item"><a href="mailto:%1$s">%1$s</a></li>', 'email_address', 'options');
        ZUP::optional_field('<li class="c_footer-navigation__item"><a class="c_icomoon c_icomoon--instagram" href="%s" target="_blank"></a></li>', 'instagram_link', 'options');
        ZUP::optional_field('<li class="c_footer-navigation__item"><a class="c_icomoon c_icomoon--twitter" href="%s" target="_blank"></a></li>', 'twitter_link', 'options');
        ZUP::optional_field('<li class="c_footer-navigation__item"><a class="c_icomoon c_icomoon--linkedin" href="%s" target="_blank"></a></li>', 'linkedin_link', 'options');
        ?>

      </ul>
    </nav>
    <p class="c_footer__smallprint"><?= nl2br(get_field('company_info', 'options')); ?></p>
  </div>
  <div class="c_gutter c_gutter--right" id="gutter-menu" aria-hidden="true">

    <?php get_template_part("template-parts/navigation", "gutter"); ?>

  </div>
</footer>
