<?php defined('ABSPATH') || exit; ?>

<div class="c_branding <?php echo !is_front_page() ? 'c_branding--inline' : ''; ?>">
  <a class="c_branding__link" href="<?= site_url(); ?>">
    <svg class="c_branding__logo" viewBox="0 0 594 157" xmlns="http://www.w3.org/2000/svg" fill="currentColor" fill-rule="evenodd" clip-rule="evenodd">
      <path d="M0 154.618h102.447v-27.047H37.47L99.414 21.854V0H3.032v27.047h58.696L0 132.764v21.854zM128.018 0v96.808c0 21.23 1.947 30.979 7.574 39.637 9.522 14.28 24.671 20.555 49.592 20.555 23.621 0 37.472-5.193 47.644-17.742 7.358-9.088 9.522-19.27 9.522-42.45V0h-33.328v96.808c0 11.049-.65 17.114-2.601 21.664-3.467 8.448-9.102 11.698-21.237 11.698-11.269 0-16.904-2.816-20.588-9.965-2.384-4.766-3.251-10.832-3.251-23.397V0h-33.327zM309.709 100.687h19.506c20.806 0 32.277-3.029 40.933-10.602 9.739-8.439 13.851-20.346 13.851-39.85 0-18.204-3.463-29.247-11.687-37.902C363.656 3.462 351.753 0 329.215 0h-52.834v154.618h33.328v-53.931zm-.65-26.614V26.397h20.372c15.605 0 21.457 6.501 21.457 23.838s-5.852 23.838-21.457 23.838h-20.372z" fill-rule="nonzero"/>
      <g class="c_branding__logomark">
        <circle cx="477" cy="26" r="26"/>
        <circle cx="477" cy="130" r="26"/>
        <circle cx="568" cy="78" r="26"/>
      </g>
    </svg>
  </a>

  <?php
  if (is_front_page()) {
    printf('<p class="c_branding__strapline">%s</p>', get_bloginfo('description'));
  }
  ?>

</div>
