<?php defined('ABSPATH') || exit;

class Zup_ACF {
	private $menu_order = 0;

	function __construct() {
		$this->load_acf();

		add_filter('acf/settings/dir', [$this, 'settings_dir']);
		add_filter('acf/settings/path', [$this, 'settings_path']);

		$this->site_meta();
	}

	function load_acf() {
		require_once(sprintf('%s/vendor/acf-pro/acf.php', get_template_directory()));
	}

	function settings_dir($dir) {
		return sprintf('%s/vendor/acf-pro/', get_template_directory_uri());
	}

	function settings_path($path) {
		return sprintf('%s/vendor/acf-pro/', get_template_directory());
	}

	function site_meta() {
    acf_add_options_page(array(
      'page_title' => 'Global Settings',
      'menu_title' => 'Global Settings',
      'menu_slug' => 'global-settings',
      'capability' => 'edit_posts',
      'redirect' => false
    ));

    acf_add_local_field_group(array(
      'key' => 'group_5c372b06872ce',
      'title' => 'Blog post',
      'fields' => array(
        array(
          'key' => 'field_5c372e79a421b',
          'label' => 'Pull out bullet points',
          'name' => 'bullet_points',
          'type' => 'repeater',
          'layout' => 'table',
          'button_label' => 'Add Bullet',
          'sub_fields' => array(
            array(
              'key' => 'field_5c372fad88d78',
              'label' => 'Heading',
              'name' => 'heading',
              'type' => 'text',
            ),
            array(
              'key' => 'field_5c372fb788d79',
              'label' => 'Body copy',
              'name' => 'body_copy',
              'type' => 'textarea',
              'rows' => 5,
            ),
          ),
        ),
      ),
      'location' => [[['param' => 'post_type', 'operator' => '==', 'value' => 'post']]],
    ));

    acf_add_local_field_group(array(
      'title' => 'Project Details',
      'fields' => array(
        array(
          'key' => 'field_5c372c3f26bdf',
          'label' => 'Image Gallery',
          'name' => 'images',
          'type' => 'gallery'
        ),
      ),
      'location' => [[['param' => 'post_type', 'operator' => '==', 'value' => 'project']]]
    ));

    acf_add_local_field_group(array(
      'key' => 'group_5bf2cc4ee20a2',
      'title' => 'Global',
      'fields' => array(
        array(
          'key' => 'field_5bf2d62fb081e',
          'label' => 'Footer CTA',
          'name' => 'cta_headline',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5bf2d56c00976',
          'label' => 'Email address',
          'name' => 'email_address',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5bf2d5a600977',
          'label' => 'Instagram link',
          'name' => 'instagram_link',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5bf2d5b400978',
          'label' => 'LinkedIn link',
          'name' => 'linkedin_link',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5bf2d5c500979',
          'label' => 'Twitter link',
          'name' => 'twitter_link',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5c33624184d88',
          'label' => 'Company info',
          'name' => 'company_info',
          'type' => 'textarea',
          'rows' => 3
        )
      ),
      'location' => [[['param' => 'options_page', 'operator' => '==', 'value' => 'global-settings']]],
      'style' => 'seamless',
    ));

    acf_add_local_field_group(array(
      'key' => 'group_5bf28d654d027',
      'title' => 'Homepage',
      'fields' => array(
        array(
          'key' => 'field_5bf28d9f59dd0',
          'label' => 'Section1',
          'type' => 'tab',
          'placement' => 'top',
        ),
        array(
          'key' => 'field_5bf28f966e873',
          'label' => 'Headline',
          'name' => 'sec1_headline',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5bf28fa56e874',
          'label' => 'Body copy',
          'name' => 'sec1_bodycopy',
          'type' => 'wysiwyg',
          'tabs' => 'all',
          'toolbar' => 'full',
        ),
        array(
          'key' => 'field_5bf28f636e871',
          'label' => 'Quote',
          'name' => 'sec1_quote',
          'type' => 'textarea',
          'rows' => 2,
        ),
        array(
          'key' => 'field_5bf28f876e872',
          'label' => 'Source of quote',
          'name' => 'sec1_cite',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5bf290226e879',
          'label' => 'Section2',
          'type' => 'tab',
          'placement' => 'top',
        ),
        array(
          'key' => 'field_5bf290006e877',
          'label' => 'Headline',
          'name' => 'sec2_headline',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5bf290f27974d',
          'label' => 'Body copy',
          'name' => 'sec2_bodycopy',
          'type' => 'wysiwyg',
          'tabs' => 'all',
          'toolbar' => 'full',
        ),
        array(
          'key' => 'field_5bf28fe86e875',
          'label' => 'Quote',
          'name' => 'sec2_quote',
          'type' => 'textarea',
          'rows' => 2,
        ),
        array(
          'key' => 'field_5bf28ff76e876',
          'label' => 'Source of quote',
          'name' => 'sec2_cite',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5bf290a87974a',
          'label' => 'Section3',
          'type' => 'tab',
          'placement' => 'top',
        ),
        array(
          'key' => 'field_5bf290bb7974c',
          'label' => 'Headline',
          'name' => 'sec3_headline',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5bf2900a6e878',
          'label' => 'Body copy',
          'name' => 'sec3_bodycopy',
          'type' => 'wysiwyg',
          'tabs' => 'all',
          'toolbar' => 'full',
        ),
        array(
          'key' => 'field_5bf290fb7974e',
          'label' => 'Quote',
          'name' => 'sec3_quote',
          'type' => 'textarea',
          'rows' => 2,
        ),
        array(
          'key' => 'field_5bf291047974f',
          'label' => 'Source of quote',
          'name' => 'sec3_cite',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5bf292221a487',
          'label' => 'Section4',
          'type' => 'tab',
          'placement' => 'top',
        ),
        array(
          'key' => 'field_5bf292371a489',
          'label' => 'Headline',
          'name' => 'sec4_headline',
          'type' => 'text',
        ),
        array(
          'key' => 'field_5bf292401a48a',
          'label' => 'Body copy',
          'name' => 'sec4_bodycopy',
          'type' => 'wysiwyg',
          'tabs' => 'all',
          'toolbar' => 'full',
        ),
        array(
          'key' => 'field_5bf292491a48b',
          'label' => 'Quote',
          'name' => 'sec4_quote',
          'type' => 'textarea',
          'rows' => 2,
        ),
        array(
          'key' => 'field_5bf292521a48c',
          'label' => 'Source of quote',
          'name' => 'sec4_cite',
          'type' => 'text',
        ),
        array(
          'key' => 'field_tab_images',
          'label' => 'Images',
          'type' => 'tab',
          'placement' => 'top',
        ),
        array(
          'key' => 'field_home_projects_image',
          'label' => 'Projects Image',
          'name' => 'home_projects_image',
          'type' => 'image',
          'return_format' => 'id',
          'preview_size' => 'medium',
        ),
        array(
          'key' => 'field_home_blog_image',
          'label' => 'Blog Image',
          'name' => 'home_blog_image',
          'type' => 'image',
          'return_format' => 'id',
          'preview_size' => 'medium',
        ),
      ),
      'location' => [[['param' => 'page', 'operator' => '==', 'value' => get_option('page_on_front')]]]
    ));
  }
}

new Zup_ACF();
