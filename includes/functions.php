<?php defined('ABSPATH') || exit;

class ZUP {
	static function is_development() {
		return !empty($_SERVER['HTTP_HOST']) && substr($_SERVER['HTTP_HOST'], 0, 8) == 'www.dev.';
	}

	static function is_production() {
		return !self::is_development();
	}

	static function optional_field($template, $selector, $post_id = false, $format_value = true) {
		if (!function_exists('get_field')) return;
		$data = get_field($selector, $post_id, $format_value);
		if ($data) return printf($template, $data);
		return false;
	}
}
