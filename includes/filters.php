<?php defined('ABSPATH') || exit;

class ZUP_Filters {
	function __construct() {
		add_filter('acf/settings/show_admin', '__return_false');
		add_filter('excerpt_length', [$this, 'excerpt_length'], 1000);
		add_filter('excerpt_more', [$this, 'excerpt_more']);
		add_filter('get_search_form', '__return_null');
		add_filter('img_caption_shortcode_width', '__return_false');
		add_filter('jpeg_quality', [$this, 'jpeg_quality']);
		add_filter('the_content_more_link', [$this, 'the_content_more_link'], 10, 2);
	}

	function excerpt_length($length) {
		return 30;
	}

	function excerpt_more() {
		return '&hellip;';
	}

	function jpeg_quality(){
		return 85;
	}

	function the_content_more_link($link, $text) {
		return false;
	}
}

new ZUP_Filters();
