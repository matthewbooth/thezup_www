<?php defined('ABSPATH') || exit;

class ZUP_Actions {
	function __construct() {
		add_action('admin_menu', [$this, 'remove_admin_items']);
		add_action('after_setup_theme', [$this, 'add_image_sizes']);
		add_action('after_setup_theme', [$this, 'add_theme_support']);
		add_action('wp_head', [$this, 'wibble']);
		add_action('init', [$this, 'head_cleanup']);
		add_action('init', [$this, 'modify_post_types']);
		add_action('init', [$this, 'register_post_type'], 1);
		add_action('parse_query', [$this, 'disable_search']);
		add_action('pre_get_posts', [$this, 'modify_default_query']);
		add_action('template_redirect', [$this, 'redirect_https'], 1);
		add_action('wp_before_admin_bar_render', [$this, 'remove_admin_bar_items']);
		add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);
		add_action('wp_enqueue_scripts', [$this, 'enqueue_styles']);
	}

	function wibble() {
		if (is_single()) {
			printf("<style>.c_header{background-image:url('%s')}</style>", get_the_post_thumbnail_url(get_the_ID(), 'large'));
		} elseif (is_front_page()) {
			printf("<style>.c_header{background-image: url('%s/public/images/home_header_background.jpg');}</style>", get_template_directory_uri());
		}
	}

	function remove_admin_items() {
		remove_menu_page('edit-comments.php');
	}

	function add_image_sizes() {
		global $content_width;

		add_image_size('post_preview', 1024, 683, 1);

		update_option('thumbnail_size_w', 150);
		update_option('thumbnail_size_h', 150);
		update_option('medium_size_w', 480);
		update_option('medium_size_h', 0);
		update_option('medium_large_size_w', 1024);
		update_option('medium_large_size_h', 0);
		update_option('large_size_h', $content_width);
		update_option('large_size_w', 0);
	}

	function add_theme_support() {
		add_theme_support('post-thumbnails');
		add_theme_support('title-tag');
	}

	function head_cleanup() {
		remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
		remove_action('wp_head', 'feed_links', 2);
		remove_action('wp_head', 'feed_links_extra', 3);
		remove_action('wp_head', 'index_rel_link');
		remove_action('wp_head', 'parent_post_rel_link', 10, 0);
		remove_action('wp_head', 'print_emoji_detection_script', 7);
		remove_action('wp_head', 'rest_output_link_wp_head');
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'start_post_rel_link', 10, 0);
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'wp_oembed_add_discovery_links');
		remove_action('wp_head', 'wp_oembed_add_host_js');
		remove_action('wp_head', 'wp_resource_hints', 2);
		remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
		remove_action('wp_print_styles', 'print_emoji_styles');
	}

	function modify_post_types() {
		$post_types = get_post_types();

		foreach ($post_types as $post_type) {
			if (post_type_supports($post_type, 'comments')) {
				remove_post_type_support($post_type, 'comments');
				remove_post_type_support($post_type, 'trackbacks');
			}
		}
	}

	function register_post_type() {
		$project = array(
			'labels' => array(
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Project',
				'all_items' => 'All Projects',
				'edit_item' => 'Edit Project',
				'menu_name' => 'Projects',
				'name' => 'Projects',
				'new_item' => 'New Project',
				'not_found' => 'No projects found',
				'not_found_in_trash' => 'No projects found in trash',
				'search_items' => 'Search Projects',
				'singular_name' => 'Project',
				'view_item' => 'View Project'
			),
			'has_archive' => 'projects',
			'menu_icon' => 'dashicons-heart',
			'menu_position' => 25,
			'public' => true,
			'show_in_rest' => true,
			'supports' => array('title', 'editor', 'excerpt', 'thumbnail')
		);

		$service = array(
			'labels' => array(
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Service',
				'all_items' => 'All Services',
				'edit_item' => 'Edit Service',
				'menu_name' => 'Services',
				'name' => 'Services',
				'new_item' => 'New Service',
				'not_found' => 'No services found',
				'not_found_in_trash' => 'No services found in trash',
				'search_items' => 'Search Services',
				'singular_name' => 'Service',
				'view_item' => 'View Service'
			),
			'has_archive' => 'services',
			'menu_icon' => 'dashicons-archive',
			'menu_position' => 25,
			'public' => false,
			'show_ui' => true,
			'show_in_rest' => true,
			'supports' => array('title', 'page-attributes')
		);

		register_post_type('project', $project);
		register_post_type('service', $service);
	}

	function disable_search($query, $error = true) {
		if (is_search() && !$query->is_admin) {
			$query->is_search = false;
			$query->query_vars['s'] = false;
			$query->query['s'] = false;

			if ($error == true) {
				$query->is_404 = true;
			}
		}
	}

	function modify_default_query($query) {
		if ($query->is_main_query() && !is_admin()) {
			$query->set('posts_per_page', -1);
		}
	}

	function redirect_https() {
		if (!is_ssl() && ZUP::is_production() && !is_admin()) {
			$https_url = sprintf('https://%s%s', $_SERVER['SERVER_NAME'], $_SERVER['REQUEST_URI']);
			wp_redirect($https_url, 301);
			exit();
		}
	}

	function remove_admin_bar_items() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('comments');
		$wp_admin_bar->remove_menu('customize');
		$wp_admin_bar->remove_node('new-media');
	}

	function enqueue_scripts() {
		if (!is_admin()){
			wp_deregister_script('jquery');
		}
	}

	function enqueue_styles() {
		wp_dequeue_style('wp-block-library');
	}
}

new ZUP_Actions();
