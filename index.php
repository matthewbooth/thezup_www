<?php defined('ABSPATH') || exit;

get_header();
?>

<main class="c_main">
	<section class="c_section">
		<div class="l_container">
			<header class="c_page-header">
				<h1><?= get_the_title(get_option('page_for_posts')); ?></h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, molestias enim quas sint dicta veniam perspiciatis sapiente nesciunt magnam!</p>
			</header>
			<div class="l_posts-grid">

				<?php while (have_posts()): the_post(); ?>

					<div class="l_posts-grid__item">

						<?php
						if ($wp_query->current_post == 0) {
							get_template_part("template-parts/post/loop");
						} else {
							get_template_part("template-parts/post/loop", "small");
						}
						?>

					</div>

				<?php endwhile; ?>

			</div>
		</div>
	</section>

	<?php get_template_part("template-parts/cta"); ?>

</main>

<?php
get_template_part("template-parts/footer");
get_footer();
?>
