<?php defined('ABSPATH') || exit;

// Vendor
include_once('vendor/autoload.php');

// Includes
include_once('includes/acf.php');
include_once('includes/actions.php');
include_once('includes/filters.php');
include_once('includes/functions.php');

// Miscellaneous
if (!isset($content_width)) {
	$content_width = 1420;
}
