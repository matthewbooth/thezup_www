<?php defined('ABSPATH') || exit;

get_header();
?>

<main class="c_main">
	<section class="c_section">
		<div class="l_container">
			<header class="c_page-header">
				<h1>Projects</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, molestias enim quas sint dicta veniam perspiciatis sapiente nesciunt magnam!</p>
			</header>
		</div>
  	<div class="l_container l_container--wide">
			<div class="l_projects-grid">

				<?php
				while (have_posts()) {
					the_post();
					get_template_part("template-parts/project/loop");
				}
				?>


			</div>
		</div>
	</section>

	<?php get_template_part("template-parts/cta"); ?>

</main>

<?php
get_template_part("template-parts/footer");
get_footer();
?>
