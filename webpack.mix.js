const mix = require("laravel-mix");

mix
  .disableNotifications()
  .setPublicPath("public")
  .setResourceRoot("../")
  .copy("resources/fonts/*.*", "public/fonts")
  .copy("resources/images/*.*", "public/images")
  .sass("resources/styles/style.scss", "public/styles")
  .js("resources/scripts/main.js", "public/scripts");
