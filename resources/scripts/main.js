import AOS from "aos";

const html = document.querySelector("html");
html.classList.remove("no-js");
html.classList.add("js");

document.addEventListener("DOMContentLoaded", function() {
  setTimeout(() => document.querySelector("body").classList.add("loaded"), 100);

  document.querySelectorAll("blockquote").forEach(node => node.setAttribute("data-aos", "fade-up"));
  document.querySelectorAll(".c_callout").forEach(node => node.setAttribute("data-aos", "fade-up"));

  AOS.init({
    debounceDelay: 50,
    throttleDelay: 99,
    offset: 200,
    delay: 100,
    duration: 400,
    once: true
  });

  // Gutter Toggles
  const gutterToggles = [...document.querySelectorAll(".js_gutter-toggle")];
  gutterToggles.forEach(function(toggle) {
    var targetGutter = document.querySelector(".c_gutter#" + toggle.getAttribute("data-target"));
    toggle.addEventListener("click", function(event) {
      targetGutter.classList.toggle("c_gutter--reveal");
    });
  });
});
