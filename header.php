<?php defined('ABSPATH') || exit; ?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="stylesheet" type="text/css" href="https://use.typekit.net/xsd5zgq.css"/>
    <link rel="stylesheet" type="text/css" href="<?php printf('%s/public/styles/style.css?ver=%s', get_template_directory_uri(), wp_get_theme()->Version); ?>"/>

    <script type="text/javascript" src="<?php printf('%s/public/scripts/main.js?ver=%s', get_template_directory_uri(), wp_get_theme()->Version); ?>"></script>

    <?php wp_head(); ?>

  </head>
  <body <?php body_class(); ?>>
    <div class="c_header <?php echo is_single() || is_front_page() ? 'c_header--fullheight' : ''; ?>">

      <?php
      get_template_part("template-parts/banner");
      if (is_single() || is_front_page()) get_template_part("template-parts/hero");
      ?>

    </div>
