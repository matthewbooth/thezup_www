<?php defined('ABSPATH') || exit;

$latest_posts = new WP_Query(array(
	'posts_per_page' => 2,
	'post_type' => 'post'
));

$latest_projects = new WP_Query(array(
	'posts_per_page' => 3,
	'post_type' => 'project'
));

$services = new WP_Query(array(
	'post_per_page' => -1,
	'post_type' => 'service',
	'order' => 'ASC',
	'orderby' => 'menu_order'
));

// sec4_image
// sec4_headline
// sec4_bodycopy
// sec4_quote
// sec4_cite

get_header();
the_post();
?>

<main class="c_main">
	<section class="c_section">
		<div class="l_container">
			<h2><?php the_field('sec1_headline'); ?></h2>

			<?php the_field('sec1_bodycopy'); ?>

			<blockquote>
				<p><?php the_field('sec1_quote'); ?></p>
				<cite><?php the_field('sec1_cite'); ?></cite>
			</blockquote>
		</div>
	</section>
	<section class="c_section c_section--alt">
		<div class="l_container">

			<!-- sec2_image -->

			<h2><?php the_field('sec2_headline'); ?></h2>

			<?php the_field('sec2_bodycopy'); ?>

			<blockquote>
				<p><?php the_field('sec2_quote'); ?></p>
				<cite><?php the_field('sec2_cite'); ?></cite>
			</blockquote>

			<?php if ($services->have_posts()): ?>

				<div class="c_service-list">
					<h2 class="c_service-list__title">Services:</h2>
					<ul class="c_service-list__list">

						<?php while ($services->have_posts()): $services->the_post(); ?>

						<li class="c_service-list__item" data-aos="fade" data-aos-delay="<?= $services->current_post * 50; ?>"><?php the_title(); ?>.</li>

						<?php endwhile; wp_reset_query(); ?>

					</ul>
				</div>

			<?php endif; ?>

		</div>
	</section>
	<section class="c_section">
		<div class="l_container">

			<!-- sec3_image -->

			<h2><?php the_field('sec3_headline'); ?></h2>

			<?php the_field('sec3_bodycopy'); ?>

			<blockquote>
				<p><?php the_field('sec3_quote'); ?></p>
				<cite><?php the_field('sec3_cite'); ?></cite>
			</blockquote>
		</div>
	</section>
	<!-- <section class="c_section c_section--image" style="background-image:url('<?= wp_get_attachment_image_src(get_field('home_projects_image'), 'large')[0]; ?>"></section> -->

	<?php if ($latest_projects->have_posts()): ?>

		<section class="c_section c_section--alt">
			<div class="l_container">
				<header class="c_page-header">
					<h2>Latest Projects</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, molestias enim quas sint dicta veniam perspiciatis sapiente nesciunt magnam!</p>
				</header>
			</div>
			<div class="l_container l_container--wide">
				<div class="l_projects-grid">

					<?php
					while ($latest_projects->have_posts()) {
						$latest_projects->the_post();
						get_template_part("template-parts/project/loop");
					}
					?>

				</div>
			</div>
			<div class="l_container">
				<a class="e_button" href="<?= get_post_type_archive_link('project'); ?>">View all projects</a>
			</div>
		</section>

	<?php endif; ?>

	<!-- <section class="c_section c_section--image" style="background-image:url('<?= wp_get_attachment_image_src(get_field('home_blog_image'), 'large')[0]; ?>"></section> -->

	<?php if ($latest_posts->have_posts()): ?>

		<section class="c_section c_section--insights">
			<div class="l_container">
				<header class="c_page-header">
					<h2>Latest <?= get_the_title(get_option('page_for_posts')); ?></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, molestias enim quas sint dicta veniam perspiciatis sapiente nesciunt magnam!</p>
				</header>
				<div class="l_related">

					<?php
					while ($latest_posts->have_posts()) {
						$latest_posts->the_post();
						get_template_part("template-parts/post/loop");
					}
					wp_reset_query();
					?>

				</div>
				<a class="e_button" href="<?php the_permalink(get_option('page_for_posts')); ?>">View all insights</a>
			</div>
		</section>

	<?php endif; ?>

	<?php get_template_part("template-parts/cta"); ?>

</main>

<?php
get_template_part("template-parts/footer");
get_footer();
?>
